const initialState = {
    value: '',
};


const rootreducer = (state= initialState, action) => {
    switch (action.type) {
        case 'AddElem':
            return{
                ...state,
                value: state.value + action.value,
            };
        case 'DELETE':
            const newElem = state.value.substr(0, state.value.length -1);
            return{
                ...state,
                value: newElem,
            };
        case 'CLEAR':
            return{
                ...state,
                value: '',
            };
        case 'CHANGE':
            return{
                ...state,
                value: String(eval(state.value)),
            };
        default:
            return state;

    }
};

export default rootreducer;