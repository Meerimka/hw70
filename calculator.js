import React, {Component} from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import {connect} from 'react-redux';
import {AddElem} from './reducer';



class Calculator extends Component {
    render() {
        const buttons1= ['1','2','3'];
        const buttons2= ['4','5','6'];
        const buttons3= ['7','8','9'];
        const buttons4= ['0'];

        return (
            <View style={styles.container}>

                <View  style={styles.ValueBox}>
                    <Text style={styles.ValueStyle}>{this.props.value}</Text>
                </View>
                <View style={styles.tools}>
                    <TouchableOpacity style={styles.subTool}>
                        <View>
                            <Text style={styles.textStyle} onPress={this.props.CLEAR}>AC</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.subTool}>
                        <View>
                            <Text style={styles.textStyle} onPress={() => this.props.AddElem('-')}>-</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.subTool}>
                        <View>
                            <Text style={styles.textStyle} onPress={() => this.props.AddElem('+')}>+</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.subTool}>
                        <View>
                            <Text style={styles.textStyle} onPress={() => this.props.AddElem('/')}>	&divide;</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.subTool}>
                        <View>
                            <Text style={styles.textStyle} onPress={() => this.props.AddElem('*')}>&times;</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.subTool}>
                        <View >
                            <Text style={styles.textStyle} onPress={this.props.DELETE}>&lt;</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.btnBox}>
                 {buttons1.map((btn, index) =>{
                    return(
                        <TouchableOpacity onPress={() => this.props.AddElem(btn)} style={styles.button} key={index}>
                            <View>
                                <Text style={styles.textStyle}>{btn}</Text>
                            </View>
                        </TouchableOpacity>
                    )
                })}
                </View>
                <View style={styles.btnBox}>
                    {buttons2.map((btn, index) =>{
                        return(
                            <TouchableOpacity onPress={() => this.props.AddElem(btn)} style={styles.button} key={index}>
                                <View>
                                    <Text style={styles.textStyle}>{btn}</Text>
                                </View>
                            </TouchableOpacity>
                        )
                    })}
                </View>
                <View style={styles.btnBox}>
                    {buttons3.map((btn, index) =>{
                        return(
                            <TouchableOpacity  onPress={() => this.props.AddElem(btn)} style={styles.button} key={index}>
                                <View>
                                    <Text style={styles.textStyle}>{btn}</Text>
                                </View>
                            </TouchableOpacity>
                        )
                    })}
                </View>
                <View style={styles.btnBox}>
                    {buttons4.map((btn, index) =>{
                        return(
                            <TouchableOpacity onPress={() => this.props.AddElem(btn)} style={styles.button} key={index}>
                                <View>
                                    <Text style={styles.textStyle}>{btn}</Text>
                                </View>
                            </TouchableOpacity>
                        )
                    })}
                </View>
                <TouchableOpacity style={styles.equal} onPress={this.props.CHANGE}>
                    <View>
                        <Text style={styles.textStyle}>=</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}


const mapStateToProps = state => ({
    value:state.value,
});

const mapDispatchToProps = dispatch => ({
    AddElem: (value) => dispatch( {type: 'AddElem', value}),
    DELETE: ()=>dispatch({type:'DELETE'}),
    CLEAR:()=>dispatch({type:'CLEAR'}),
    CHANGE:()=>dispatch({type:'CHANGE'}),
});


const styles = StyleSheet.create({
    container: {
        backgroundColor: 'skyblue',
        position: 'relative',
    },
    tools:{
        flex:1,
        flexDirection:'row',
        alignItems: 'stretch',
        justifyContent: 'space-between',
        width: 340,
        marginTop: 50,
        marginBottom: 60,
        marginLeft:5,
    },
    subTool:{
        paddingTop: 10,
        backgroundColor: 'steelblue',
        width: 50,
        height: 50,
        borderRadius: 20,
    },
    equal:{
        position: 'absolute',
        bottom: 15,
        right: 30,
        width: 50,
        height: 80,
        backgroundColor:'steelblue',

    },
    btnBox: {
       flex: 1,
        backgroundColor:'blue',
        flexDirection: 'row',
        width: 350,
        justifyContent: 'space-between',
        marginBottom: 100,
    },
    button: {
        margin: 5,
        flex:0.8,
        backgroundColor: 'steelblue',
        width: 80,
        height: 80,
        justifyContent: 'center',
    },
    textStyle:{
        textAlign:'center',
        textTransform: 'uppercase',
        color: '#fff',
        fontSize: 25,
    },
    ValueBox:{
        height:50
    },
    ValueStyle:{
        color: '#fff',
        fontSize: 25,
    }

});


export default connect(mapStateToProps,mapDispatchToProps)(Calculator);