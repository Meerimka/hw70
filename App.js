
import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import rootreducer from './reducer';
import Calculator from './calculator';



const store = createStore(rootreducer, applyMiddleware(thunkMiddleware));

const index = () => (
    <Provider store={store}>
        <View style={styles.container}>
            <Calculator />
        </View>
    </Provider>
);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        marginTop: 50
    }
});

export default index;
